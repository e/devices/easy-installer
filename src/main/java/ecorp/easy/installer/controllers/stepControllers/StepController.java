/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;

import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.models.steps.IStep;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author vincent Bourgmayer
 * @param <S> implementation of IStep interface
 */
public class StepController<S extends IStep> implements Initializable{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static MainWindowController parentController;

    @FXML Node uiRoot;
    protected ResourceBundle i18n;
    protected S step;   

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        i18n = rb;
        
        final String stepKey = parentController.getCurrentStepKey();
        System.out.println("stepKey = "+stepKey);
        
        step = (S) parentController.getPhone().getFlashingProcess().getSteps().get(stepKey);
        
        parentController.setNextButtonOnClickListener( ( MouseEvent event) -> {
            if(event.getEventType().equals(MouseEvent.MOUSE_CLICKED)){
                onContinueClicked();
            }
        });
    }
    
    /**
     * /!\ it must be called Before the Initialize() method!!
     * @param parentController 
     */
    public static void setParentController(MainWindowController parentController) {
        StepController.parentController = parentController;
    }

    /**
     * Behaviour for when user click on "continue button"
     * defined mostly for subclass overriding
     */
    protected void onContinueClicked(){
        onStepEnd();
    }
    
    /**
     * Code executed when the step is over
     * Mainly load the next step
     * apply UI effect like fade in/out
     */
    protected void onStepEnd(){
        System.out.println("onStepEnd() load: "+step.getNextStepKey());
        parentController.setCurrentStepKey(step.getNextStepKey());
        if(step.getNextStepKey().equals(IStep.LAST_STEP_KEY)){
            parentController.getPhone().setFlashed();
        }
        parentController.loadSubScene();
        

        //Question is: Should I call this only if we are at the last step
        //Then How to check that from here ?
        //Or should I always reset behaviour.. As It is always override by new step
        //this looks like useless call each time except for the latest one...
        //Answer: this can go like this for now, and be handled later in the refactoring process
        //@Todo
        parentController.resetNextButtonEventHandler();
    }
}
