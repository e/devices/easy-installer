@echo off

:: Copyright (C) 2022 ECORP SAS - Author: Frank Preel

:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.

:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.

:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID ID of the device to wait
:: $2: ADB_FOLDER_PATH: the path where runnable adb is stored

:: Exit status
:: - 0 : success
:: - 101 : adb wait sideload failed

SET DEVICE_ID=%~1
SET ADB_FOLDER_PATH=%~2

SET ADB_PATH="%ADB_FOLDER_PATH%adb"
echo "waiting for recovery"

echo %ADB_PATH% -s %DEVICE_ID% wait-for-sideload

%ADB_PATH% -s %DEVICE_ID% wait-for-sideload
if not errorlevel 1 (
  echo "device found in recovery"
  exit /b 0
) ELSE (
  echo "device not detected in recovery"
  exit /b 101
)
