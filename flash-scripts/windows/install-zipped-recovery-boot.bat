@echo off

::  Copyright (C) 2023 Murena SAS - Author: SahilSonar
::  This program is free software: you can redistribute it and/or modify
::  it under the terms of the GNU General Public License as published by
::  the Free Software Foundation, either version 3 of the License, or
::  (at your option) any later version.
::  This program is distributed in the hope that it will be useful,
::  but WITHOUT ANY WARRANTY; without even the implied warranty of
::  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
::  GNU General Public License for more details.
::  You should have received a copy of the GNU General Public License
::  along with this program.  If not, see <https://www.gnu.org/licenses/>.

::  Parameters
::  %1: TWRP_IMAGE_PATH need twrp path (%TWRP_FOLDER%\%TWRP%)
::  %2: FASTBOOT_FOLDER_PATH needs fastboot binary path 
::  %3: JAVA_FOLDER_PATH needs java binary path

::  Exit status
::  - 0 : Recovery installed
::  - 101 : TWRP_IMAGE_PATH missing
::  - 103 : Failed to unzip

SET "TWRP_IMAGE_PATH=%~1"
SET "OUTPUT_FOLDER=%~dp1"
SET "FASTBOOT_FOLDER_PATH=%~2"
SET "FASTBOOT_PATH=%FASTBOOT_FOLDER_PATH%\fastboot"
SET "JAVA_FOLDER_PATH=%~3"
SET "JAR_PATH=%JAVA_FOLDER_PATH%\bin\jar"

:: Ensure there's no trailing backslash in FASTBOOT_FOLDER_PATH
IF "%FASTBOOT_FOLDER_PATH:~-1%"=="\" SET "FASTBOOT_FOLDER_PATH=%FASTBOOT_FOLDER_PATH:~0,-1%"
SET "FASTBOOT_PATH=%FASTBOOT_FOLDER_PATH%\fastboot"

echo fastboot path: %FASTBOOT_PATH%

IF NOT DEFINED TWRP_IMAGE_PATH (
  echo TWRP Image path is empty
  exit /b 101
)

:: Delete any existing *.img files in OUTPUT_FOLDER
del /Q "%OUTPUT_FOLDER%\*.img"
del /Q "%OUTPUT_FOLDER%\*.img.sha256sum"

:: Use jar tool to extract all .img files from the zip archive
cd /d "%OUTPUT_FOLDER%" || exit /b 102
"%JAR_PATH%" -x -v -f "%TWRP_IMAGE_PATH%"

IF %ERRORLEVEL% NEQ 0 (
  exit /b 103
)

:: Define slots to flash
SET "SLOTS=a b"

echo Flashing boot
for %%I in ("%OUTPUT_FOLDER%\recovery-*-*.img") do (
  for %%S in (%SLOTS%) do (
    "%FASTBOOT_PATH%" flash boot_%%S "%%~I"
  )
)

echo Flashing dtbo
for %%I in ("%OUTPUT_FOLDER%\dtbo-*-*.img") do (
  for %%S in (%SLOTS%) do (
    "%FASTBOOT_PATH%" flash dtbo_%%S "%%~I"
  )
)
