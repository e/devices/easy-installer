@echo off

:: Coyright (C) 2022 ECORP SAS - Author: Frank Preel

:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.

:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.

:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device id
:: $2: ARCHIVE_PATH path to archive
:: $3: fastboot folder path
:: $4: Java folder path


:: Exit status
:: - 0 : device flashed
:: - 1 : generic error
:: - 10: can't unpack system.img
:: - 11: can't wipe userdata
:: - 12: can't wipe metadata
:: - 13: can't active partition
:: - 20-34 : see partition_name index below
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing
:: - 103 : fastboot folder path missing

SET partition_name[0]=boot_a
SET partition_name[1]=dtbo_a
SET partition_name[2]=vbmeta_a
SET partition_name[3]=vbmeta_system_a
SET partition_name[4]=vbmeta_vendor_a
SET partition_name[5]=super
SET partition_name[6]=lk_a
SET partition_name[7]=logo
SET partition_name[8]=preloader_a
SET partition_name[9]=tee_a
SET partition_name[10]=gz_a
SET partition_name[11]=sspm_a
SET partition_name[12]=scp_a
SET partition_name[13]=spmfw_a
SET partition_name[14]=md1img_a

SET partition_image[0]=boot.img
SET partition_image[1]=dtbo.img
SET partition_image[2]=vbmeta.img
SET partition_image[3]=vbmeta_system.img
SET partition_image[4]=vbmeta_vendor.img
SET partition_image[5]=super.img
SET partition_image[6]=lk.img
SET partition_image[7]=logo.bin
SET partition_image[8]=preloader_emerald.img
SET partition_image[9]=tee.img
SET partition_image[10]=gz.img
SET partition_image[11]=sspm.img
SET partition_image[12]=scp.img
SET partition_image[13]=spmfw.img
SET partition_image[14]=md1img.img

SET partition_error[0]=20
SET partition_error[1]=21
SET partition_error[2]=22
SET partition_error[3]=23
SET partition_error[4]=24
SET partition_error[5]=25
SET partition_error[6]=26
SET partition_error[7]=27
SET partition_error[8]=28
SET partition_error[9]=29
SET partition_error[10]=30
SET partition_error[11]=31
SET partition_error[12]=32
SET partition_error[13]=33
SET partition_error[14]=34

SET DEVICE_ID=%~1
SET ARCHIVE_PATH=%~2
SET FASTBOOT_FOLDER_PATH=%~3
SET JAVA_FOLDER_PATH=%~4

IF not defined  %DEVICE_ID (
  exit /b 101
)
IF not defined  %ARCHIVE_PATH (
  exit /b 102
)
IF not defined  %FASTBOOT_FOLDER_PATH (
  exit /b 103
)
IF not defined  %JAVA_FOLDER_PATH (
  exit /b 104
)

:: Build fastboot path
SET FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"

:: Build java jar path
SET JAR_PATH="%JAVA_FOLDER_PATH%/bin/jar"

:: Build archive folder path
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH=%%~dpa"
	echo %ARCHIVE_FOLDER_PATH%
)

:: unzip for system.img
cd "%ARCHIVE_FOLDER_PATH%"
%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 10 )
echo "unpacked archive"
timeout 1 >nul 2>&1


:: Wipe user data
%FASTBOOT_PATH% erase userdata
if errorLevel 1 ( exit /b 11 )
echo "user data wiped"
timeout 5 >nul 2>&1

:: Wipe meta data
%FASTBOOT_PATH% format md_udc
if errorLevel 1 ( exit /b 12 )
echo "meta data wiped"
timeout 5 >nul 2>&1

:: Flash partition
(for /L %%i in (0,1,10) do (
	call %FASTBOOT_PATH% -s %DEVICE_ID% flash %%partition_name[%%i]%% %%partition_image[%%i]%%
	if errorLevel 1 ( exit /b %%partition_error[%%i]%% )
	timeout 1 >nul 2>&1
	call echo "Flashed %%partition_name[%%i]%% "
))

:: Activate
%FASTBOOT_PATH% -s %DEVICE_ID% --set-active=a
if errorLevel 1 ( exit /b 13 )
echo "set acivated"
timeout 5 >nul 2>&1
