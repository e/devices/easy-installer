FROM snapcore/snapcraft:stable as builder

FROM ubuntu:bionic

COPY --from=builder /snap/core /snap/core
COPY --from=builder /snap/core18 /snap/core18
COPY --from=builder /snap/snapcraft /snap/snapcraft
COPY --from=builder /snap/bin/snapcraft /snap/bin/snapcraft

# Install necessary packages
RUN apt-get update && apt-get dist-upgrade --yes && \
    apt-get install --yes snapd sudo locales wget unzip gcc nsis rsync hfsprogs hfsplus && \
    locale-gen en_US.UTF-8

# Set the proper environment
ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US:en"
ENV LC_ALL="en_US.UTF-8"
ENV PATH="/snap/bin:$PATH"
ENV SNAP="/snap/snapcraft/current"
ENV SNAP_NAME="snapcraft"
ENV SNAP_ARCH="amd64"

# Install JVM
COPY buildSrc/linux/jdk-11.0.2 /usr/lib/jdk/jdk-11.0.2
ENV JAVA_HOME=/usr/lib/jdk/jdk-11.0.2

# Download Gradle ZIP file into the container
ADD https://github.com/gradle/gradle-distributions/releases/download/v4.10.2/gradle-4.10.2-bin.zip /tmp/

# Extract Gradle
RUN mkdir -p /usr/local/gradle && \
    unzip /tmp/gradle-4.10.2-bin.zip -d /usr/local/gradle && \
    rm /tmp/gradle-4.10.2-bin.zip

# Set Gradle environment variables
ENV GRADLE_HOME=/usr/local/gradle/gradle-4.10.2
ENV PATH=$GRADLE_HOME/bin:$PATH
